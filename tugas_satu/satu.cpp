#include <iostream>
#include <math.h>


using namespace std;


struct Point {
    int x, y;
};

struct JarakPoint {
    Point p1, p2;
    double jarak;
};

double hitungJarakAntar2Point(Point p1, Point p2) {
    return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

void tampilkanJarakAntarPoint(JarakPoint arrPoint[], int sizePoint) {
    for (int i = 0; i < sizePoint; i++) 
    {
        cout<<"Jarak antar point: ("<<arrPoint[i].p1.x<<","<<arrPoint[i].p1.y<<") dan ("<<
        arrPoint[i].p2.x<<","<<arrPoint[i].p2.y<<") adalah: "<<arrPoint[i].jarak<<endl;
    }
}

int main() {
    int n, x, y;
    Point points[30];
    JarakPoint jarakPoint[1000];
    double jarakTerdekat = -1.0;

    cout<<"Masukkan jumlah point yang akan dimasukkan: "; cin>>n;

    for (int i = 0; i < n; i++)
    {
        cout<<"Masukkan nilai kordinat x index ke-"<<i+1<<": "; cin>>x;
        cout<<"Masukkan nilai kordinat y index ke-"<<i+1<<": "; cin>>y;
        struct Point point = {x, y};
        points[i] = point;
    }
    
    int loopCount = 0;
    for (int i = 0; i < n - 1; i++) 
    {
        Point p1 = points[i];
        for (int j = i + 1; j < n; j++)
        {
            Point p2 = points[j];
            double jarak = hitungJarakAntar2Point(p1, p2);
            struct JarakPoint jp = {p1, p2, jarak};
            jarakPoint[loopCount] = jp;
            
            if (jarakTerdekat == -1.0) {
                jarakTerdekat = jarak;
            } else {
                jarakTerdekat = jarakTerdekat > jarak ? jarak : jarakTerdekat;
            }

            loopCount++;
        }
    }

    tampilkanJarakAntarPoint(jarakPoint, loopCount);

    JarakPoint jp; 
    cout<<"Titik yang terdekat adalah:"<<endl;
    for (int i = 0; i < loopCount; i++) {
        if (jarakPoint[i].jarak == jarakTerdekat) {
            jp = jarakPoint[i];
            cout<<"p1:("<<jp.p1.x<<","<<jp.p1.y<<") p2:("<<jp.p2.x<<","<<jp.p2.y<<") dengan jarak:"<<jp.jarak<<endl; 
        }
    }

}