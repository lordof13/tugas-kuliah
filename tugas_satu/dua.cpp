#include <iostream>
using namespace std;


struct Pecahan {
    int pembilang, penyebut;
};
  
void insertionSort(Pecahan arr[], int n)  
{  
    int i, j;
    
    for (i = 1; i < n; i++) {
        j = i;
        while (j > 0 && (double) arr[j - 1].pembilang / arr[j - 1].penyebut > (double) arr[j].pembilang / arr[j].penyebut) {  
            Pecahan tmp = arr[j];
            arr[j] = arr[j - 1]; 
            arr[j - 1] = tmp;
            j--;
        }
    }
}

  
void printArray(Pecahan arr[], int n)  
{  
    int i;  
    for (i = 0; i < n; i++) {
        cout << arr[i].pembilang << "/"<< arr[i].penyebut << " ";
    }  
    cout << endl; 
}  
  
int main()  
{ 
    int n;
    Pecahan arrPecahan[100];
    Pecahan arrHasil[100];
    cout<<"Masukkan jumlah pecahan yang ingin diurutkan: "; cin>>n;

    int pembilang, penyebut;
    for (int i=0; i < n; i++) {
        cout<<"Masukkan pembilang ke-"<< i + 1 << ": "; cin>>pembilang;
        cout<<"Masukkan penyebut ke-"<< i + 1 << ": "; cin>>penyebut;
        struct Pecahan pec = {pembilang, penyebut};
        arrPecahan[i] = pec; 
    }
  
    insertionSort(arrPecahan, n);
    cout<<endl<<"Hasil Pengurutan: "<<endl;  
    printArray(arrPecahan, n);  
  
    return 0;  
}  