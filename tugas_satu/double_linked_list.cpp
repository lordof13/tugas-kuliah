#include<iostream>
#include <stdlib.h>

using namespace std;

// definiskan strukture dati double linked list
// right berfungsi untuk menyimpan alamat memory node selanjutnya
// left berfungsi untuk menimpan alamat memory node sebelumnya
struct Node{
	int data;
	Node * right;
	Node * left;
};

Node* SortedInsert(Node *head,int data)
{
    Node * temp = (Node*)malloc(sizeof(Node));

    temp->data = data;
    temp->right = NULL;
    temp->left = NULL;

    if (head == NULL)
    {
        head = temp;
        
        return head;
    }

    if (temp->data <= head->data)
    {
        temp->right = head;
        head->left = temp;
        head = temp;
        return head;
    }

    Node *current = head;
    while (current->right!=NULL)
    {
        if (temp->data <= current->data)
        {   
            current->left->right = temp;
            temp->left = current->left;
            temp->right = current;
            current->left = temp;
            return head;
        }

        current = current->right;
    }

    if (temp->data <= current->data)
    {   
        current->left->right = temp;
        temp->left = current->left;
        temp->right = current;
        current->left = temp;
        return head;
    }

    current->right = temp;
    temp->left = current;

    return head;
}


Node* DeleteNode( Node *head, int value )
{
    for (Node *current = head; current; )
    {
        if ( current->data == value ) 
        {
            Node *tmp = current;

            if ( current->right )
            {
                current->right->left = current->left;
            }

            if ( current->left )
            {
                current->left->right = current->right;
                current = current->right;
            }
            else
            {
                head = current->right;
                current = head;
            }

            delete tmp;
        }
        else
        {
            current = current->right;
        }            
    }

    return head;
}

void printList(Node *temp)  
{  
    while (temp != NULL)  
    {  
        cout << temp->data << " ";  
        temp = temp->right;  
    }  
    cout<<endl;
}  
 

main(){
	Node * head = SortedInsert(NULL,40);
	head = SortedInsert(head,50);
	head = SortedInsert(head,20);
	head = SortedInsert(head,20);
	head = SortedInsert(head,10);
	printList(head);
	head = DeleteNode(head,20);
	printList(head);
	head = DeleteNode(head,10);
	printList(head);
	head = DeleteNode(head,50);
	printList(head);
}
